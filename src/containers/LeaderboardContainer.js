/*
Imports
 */
// React
import { connect } from 'react-redux'
// Inner
import { setGameState } from '../actions';
import LeaderboardList from '../components/LeaderboardList';
//

/*
Functions
 */
function renderTimer(timer) {
    let min = Math.floor(timer / 60);
    let sec = Math.floor(timer - (min * 60));
    return `${min} min ${sec} sec`
}

function convertScore(soloLeaderboard) {
    return soloLeaderboard.map(score => {
        score.timer = renderTimer(score.timer)
        return score;
    })
}
//

/*
State and Actions dispatch
*/
// State
const mapStateToProps = state => {
    const tempLeaderboard = JSON.parse(JSON.stringify(state.gameLeaderboard.leaderboard.solo));
    return {
        soloLeaderboard: convertScore(tempLeaderboard)
    }
};
// Actions dispatch
const mapDispatchToProps = dispatch => {
    return {
        onBackwardClick: gameState => {
            dispatch(setGameState(gameState));
        }
    }
}
//

/*
Export
 */
const LeaderboardContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(LeaderboardList)

export default LeaderboardContainer;
//