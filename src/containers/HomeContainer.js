/*
Imports
 */
// React
import { connect } from 'react-redux'
import { setMode, initGame, setPlayerName, setGameState } from '../actions';
// Inner
import Home from '../components/Home';
//

/*
State and Action dispatch
 */
// State
const mapStateToProps = state => {
    return {
        gameMode: state.gameInfos.mode,
        playersInfos: state.gameInfos.playersInfos
    }
};
// Actions dispatch
const mapDispatchToProps = dispatch => {
    return {
        onModeClick: mode => {
            dispatch(setMode(mode));
        },
        onInitClick: cardsCount => {
            dispatch(initGame(cardsCount));
        },
        onInputChange: playersInfos => {
            dispatch(setPlayerName(playersInfos));
        },
        onLeaderboardClick: gameState => {
            dispatch(setGameState(gameState));
        }
    }
}
//

/*
Export
 */
const HomeContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Home)

export default HomeContainer;
//