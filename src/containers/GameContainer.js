/*
Import
 */
// React
import { connect } from 'react-redux';
// Inner
import { initLeaderboard } from '../actions';
import Game from '../components/Game';
//

/*
State and Actions dispatch
 */
// State
const mapStateToProps = state => {
    return {
        gameState: state.gameInfos.gameState
    }
};
// Actions dispatch
const mapDispatchToProps = dispatch => {
    return {
        onPageLoad: () => {
            dispatch(initLeaderboard());
        }
    }
}
//

/*
Export
 */
const GameContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Game)

export default GameContainer;
//