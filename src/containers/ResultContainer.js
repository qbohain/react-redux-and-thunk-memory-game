/*
Imports
 */
// React
import { connect } from 'react-redux'
// Inner
import { resetGame } from '../actions';
import Result from '../components/Result';
//

/*
Functions
 */
function renderTimer(timer) {
    let min = Math.floor(timer / 60);
    let sec = Math.floor(timer - (min * 60));
    return `${min} min ${sec} sec`
}
//

/*
State and Actions dispatch
*/
// State
const mapStateToProps = state => {
    return {
        gameMode: state.gameInfos.mode,
        timer: renderTimer(state.gameInfos.timer),
        playersInfos: state.gameInfos.playersInfos
    }
};
// Actions dispatch
const mapDispatchToProps = dispatch => {
    return {
        onHomeClick: () => {
            dispatch(resetGame());
        }
    }
}
//

/*
Export
 */
const ResultContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Result)

export default ResultContainer;
//