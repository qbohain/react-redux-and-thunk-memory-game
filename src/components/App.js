/*
Imports
 */
// React
import React from 'react';
// Inner
import GameContainer from "../containers/GameContainer";
//

/*
Component
 */
const App = () => {
  return <GameContainer/>
}
//

/*
Export
 */
export default App;
//