/*
Imports
 */
// React
import React from 'react';
import PropTypes from 'prop-types';
// Inner
import ModeButton from './ModeButton';
//

/*
Functions
 */
const handleChange = (e, gameMode, onInputChange, playersInfos) => {
    if (gameMode === 0) {
        playersInfos = { ...playersInfos, p1: { name: e.target.value } }
        onInputChange(playersInfos);
    } else if (gameMode === 1) {
        if (e.target.name === 'p1')
            playersInfos = { ...playersInfos, p1: { name: e.target.value, score: 0 } }
        else if (e.target.name === 'p2')
            playersInfos = { ...playersInfos, p2: { name: e.target.value, score: 0 } }
        onInputChange(playersInfos);
    }
}

const renderInputs = ({ onInputChange, gameMode, playersInfos }) => {
    if (gameMode === 0)
        return (
            <div>
                <p className="label">Joueur :</p>
                <input
                    onChange={e => {
                        handleChange(e, gameMode, onInputChange, playersInfos)
                    }}
                    value={playersInfos.p1.name} name='p1' type="text" required placeholder='Pseudo*' />
            </div>
        )
    else if (gameMode === 1)
        return (
            <div>
                <p className="label">Joueur 1 :</p>
                <input onChange={e => {
                    handleChange(e, gameMode, onInputChange, playersInfos)
                }} value={playersInfos.p1.name} name='p1' type="text" required placeholder='Pseudo*' /><br />
                <p className="label">Joueur 2 :</p>
                <input onChange={e => {
                    handleChange(e, gameMode, onInputChange, playersInfos)
                }} value={playersInfos.p2.name} name='p2' type="text" required placeholder='Pseudo*' /><br />
            </div>
        )
}
//

/*
Component
 */
const Home = ({ onModeClick, onInitClick, onInputChange, onLeaderboardClick, gameMode, playersInfos }) => (
    <div className="wrapper-home">
        <h1>Choix du mode</h1>
        <form
            onSubmit={e => {
                e.preventDefault();
                onInitClick(12);
            }}
        >
            <p className="label">Mode :</p>
            <ModeButton
                    onClick={() => onModeClick(0)}
                    label="1P"
                />
                <ModeButton
                    onClick={() => onModeClick(1)}
                    label="2P"
                />
                {renderInputs({ onInputChange, gameMode, playersInfos })}
                <input className="jouer-button button" type="submit" value="Jouer" />
        </form>
            <button className="leaderboard-button button" onClick={() => onLeaderboardClick(3)}>Leaderboard</button>
    </div>
        )
        //
        
        /*
        PropTypes
         */
Home.PropTypes = {
            onModeClick: PropTypes.func.isRequired,
        onInitClick: PropTypes.func.isRequired,
        onInputChange: PropTypes.func.isRequired,
        gameMode: PropTypes.number.isRequired,
    playersInfos: PropTypes.shape({
            p1: PropTypes.shape({
            name: PropTypes.string.isRequired,
        score: PropTypes.number
    }).isRequired,
        p2: PropTypes.shape({
            name: PropTypes.string.isRequired,
        score: PropTypes.number.isRequired
    }),
    roundCount: PropTypes.number
}).isRequired
}
//

/*
Export
*/
export default Home;
//