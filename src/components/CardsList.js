/*
Import
 */
// React
import React from 'react';
import PropTypes from 'prop-types';
// Inner
import Card from './Card';
//

/*
Component
 */
const CardsList = ({ cards, onCardClick }) => (
    <ul className="cards-list">
        {cards.map((card, index) => (
            <Card key={index} {...card} toggleCard={() => onCardClick(index, card.code)}/>
        ))}
    </ul>
)
//

/*
PropTypes
 */
CardsList.PropTypes = {
    cards: PropTypes.arrayOf(
        PropTypes.shape({
            suit: PropTypes.string.isRequired,
            value: PropTypes.string.isRequired,
            code: PropTypes.string.isRequired,
            images: PropTypes.shape({
                svg: PropTypes.string.isRequired,
                png: PropTypes.string.isRequired
            }).isRequired,
            image: PropTypes.string.isRequired
        }).isRequired
    ).isRequired,
    onCardClick: PropTypes.func.isRequired
}
//

/*
Export
 */
export default CardsList;
//