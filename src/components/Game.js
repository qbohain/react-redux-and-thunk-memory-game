/*
Imports
 */
// React
import React from 'react';
import PropTypes from 'prop-types';
// Inner
import HomeContainer from '../containers/HomeContainer';
import BoardContainer from '../containers/BoardContainer';
import ResultContainer from '../containers/ResultContainer';
import LeaderboardContainer from '../containers/LeaderboardContainer';
//

/*
Component
 */
export default class Game extends React.Component {

    componentDidMount() {
        this.props.onPageLoad();
    }

    renderGame() {
        switch (this.props.gameState) {
            case 0:
                return <HomeContainer/>
            case 1:
                return <BoardContainer/>
            case 2:
                return <ResultContainer/>
            case 3:
                return <LeaderboardContainer/>
            default:
                return <HomeContainer/>
        }
    }

    render() {
        return this.renderGame();
    }

}
//

/*
PropTypes
 */
Game.PropsType = {
    gameState: PropTypes.bool.isRequired,
    onPageLoad: PropTypes.func.isRequired
}
//