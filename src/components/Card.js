/*
Import
 */
// React
import React from 'react';
import PropTypes from 'prop-types';
//

/*
Functions
 */
function renderCard({ image, isSelected, isFound, toggleCard }) {
    if (!isSelected && !isFound)
        return (
            <li
                onClick={toggleCard}
                className="face-down"
            ></li>
        )
    else if (isSelected  && !isFound)
        return (
            <li
            ><img src={image} alt={image}/></li>
        )
    else if (isFound)
        return (
            <li className="found"></li>
        )
}
//

/*
Component
 */
const Card = ({ image, isSelected, isFound, toggleCard }) => (
    renderCard({ image, isSelected, isFound, toggleCard })
)
//

/*
PropTypes
 */
Card.PropTypes = {
    image: PropTypes.string.isRequired,
    toggleCard: PropTypes.func.isRequired,
    isSelected: PropTypes.bool.isRequired,
    isFound: PropTypes.bool.isRequired
}
//

/*
Export
 */
export default Card;
//