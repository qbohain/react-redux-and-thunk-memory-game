/*
Import
 */
// React
import React from 'react';
import PropTypes from 'prop-types';
// Inner
import CardsList from './CardsList';
import Infos from './Infos';
//

/*
Component
TODO: Ajouter le composant Infos pour gérer l'affichage du score / joueur courant
 */
const Board = ({ playersInfos, timer, cards, gameMode, onCardClick }) => (
    <div className="game-wrapper">
        <Infos playersInfos={playersInfos} timer={timer} gameMode={gameMode}/>
        <CardsList cards={cards} onCardClick={onCardClick}/>
    </div>
)
//

/*
PropTypes
 */
Board.PropTypes = {
    cards: PropTypes.arrayOf(
        PropTypes.shape({
            suit: PropTypes.string.isRequired,
            value: PropTypes.string.isRequired,
            code: PropTypes.string.isRequired,
            images: PropTypes.shape({
                svg: PropTypes.string.isRequired,
                png: PropTypes.string.isRequired
            }).isRequired,
            image: PropTypes.string.isRequired
        }).isRequired
    ).isRequired,
    timer: PropTypes.number.isRequired,
    gameMode: PropTypes.number.isRequired,
    playersInfos: PropTypes.shape({
        p1: PropTypes.shape({
            name: PropTypes.string.isRequired,
            score: PropTypes.number
        }).isRequired,
        p2: PropTypes.shape({
            name: PropTypes.string.isRequired,
            score: PropTypes.number.isRequired
        }),
        roundCount: PropTypes.number
    }).isRequired,
    onCardClick: PropTypes.func.isRequired
}
//

/*
Export
 */
export default Board;
//